package tn.esprit.erpschool.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import tn.esprit.erpschool.entites.Student;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.IStudentCrudLocal;

@RequestScoped
@ManagedBean(name = "studentConverter")
public class StudentConverter implements Converter {

	@EJB
	IStudentCrudLocal studentService;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		String[] names = arg2.split("-");
		try {
			return studentService.findStudentByFirstNameAndLastName(names[0], names[1]);
		} catch (MoreThanOneResultException | NoResultFoundException e) {
			return null;
		}

	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		Student student = (Student)arg2;
		return student.getFirstName()+"-"+student.getLastName();
	}

}
