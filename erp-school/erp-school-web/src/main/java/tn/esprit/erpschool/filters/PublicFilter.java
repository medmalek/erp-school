package tn.esprit.erpschool.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.esprit.erpschool.entites.Student;
import tn.esprit.erpschool.entites.Teacher;
import tn.esprit.erpschool.managedbeans.AuthenticationBean;

//@WebFilter(filterName = "publicFilter", urlPatterns = "/public/*")
public class PublicFilter implements Filter {

	public PublicFilter() {
		// TODO Auto-generated constructor stub
	}

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		AuthenticationBean authenticationBean = (AuthenticationBean) req
				.getSession().getAttribute("authenticationBean");

		if (authenticationBean != null) {
			if (authenticationBean.isLoggedIn()) {
				if (authenticationBean.getPerson() instanceof Student) {
					res.sendRedirect(req.getContextPath()
							+ "/student/homePage.jsf");
				}
				if (authenticationBean.getPerson() instanceof Teacher) {
					res.sendRedirect(req.getContextPath()
							+ "/teacher/homePage.jsf");
				}
			} else {
				chain.doFilter(req, res);
			}

		}
		chain.doFilter(req, res);

	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
