package tn.esprit.erpschool.managedbeans;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import tn.esprit.erpschool.entites.Person;
import tn.esprit.erpschool.entites.Student;
import tn.esprit.erpschool.entites.Teacher;
import tn.esprit.erpschool.exceptions.BadCredentialsException;
import tn.esprit.erpschool.services.contracts.IPersonServiceLocal;

@SessionScoped
@ManagedBean(name = "authenticationBean")
public class AuthenticationBean {

	@EJB
	private IPersonServiceLocal personService;
	private Person person = new Person();
	private boolean loggedIn;

	public String doAuthenticate() {
		try {
			Object object = personService.findPersonByCredentials(
					person.getEmail(), person.getPassword());
			if (object instanceof Student) {
				person = (Student) object;
				loggedIn = true;
				return "/student/homePage?faces-redirect=true";
			}
			if (object instanceof Teacher) {
				person = (Teacher) object;
				loggedIn = true;
				return "/teacher/homePage?faces-redirect=true";
			}
		} catch (BadCredentialsException e) {
			return "login?faces-redirect=true";
		}
		return null;
	}

	public String doLogout() {
		FacesContext.getCurrentInstance().getExternalContext()
				.invalidateSession();
		return "/public/login?faces-redirect=true";
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	

}
