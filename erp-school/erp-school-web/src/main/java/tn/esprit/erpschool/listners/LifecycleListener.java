package tn.esprit.erpschool.listners;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class LifecycleListener implements PhaseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3190564129550138829L;

	@Override
	public void afterPhase(PhaseEvent event) {
		System.out.println("JSF a cloturé la phase : " + event.getPhaseId());
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		System.out.println("JSF est entrain de préprer la phase : "
				+ event.getPhaseId());

	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

}