package tn.esprit.erpschool.validators;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.IPersonServiceLocal;

@RequestScoped
@ManagedBean(name="emailValidator")
public class EmailValidator implements Validator {

	@EJB
	IPersonServiceLocal personSevices;

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String email = (String) value;
		try {
			if (personSevices.isPersonEmailUnique(email)) {
				return;
			} else {
				throw new ValidatorException(
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"E-mail exists !",
								"Please log in with this e-mail or choose another one to register with"));
			}
		} catch (MoreThanOneResultException | NoResultFoundException e) {
			throw new ValidatorException(
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"E-mail exists !",
							"Please log in with this e-mail or choose another one to register with"));
		}

	}

}
