package tn.esprit.erpschool.managedbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import tn.esprit.erpschool.entites.ClassRoom;
import tn.esprit.erpschool.entites.Classe;
import tn.esprit.erpschool.entites.Student;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.IClasseCrudLocal;
import tn.esprit.erpschool.services.contracts.IStudentCrudLocal;

@RequestScoped
@ManagedBean
public class ClasseBean {

	@EJB
	IClasseCrudLocal classeService;
	
	@EJB
	IStudentCrudLocal studentService;

	private List<Classe> classes;
	
	private List<Student> students;

	private Classe classe = new Classe();
	
	private Student selectedStudent;

	private boolean showForm = false;

	@PostConstruct
	private void init() {
		classes = classeService.findAllClasses();
		students = studentService.findAllStudents();
	}

	public String doDeleteClasse(Classe classe) {

		try {
			classeService.deleteClasse(classe.getId());
		} catch (NoResultFoundException e) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Error while fetching classe",
							"Please verify the classe id"));
			return null;

		}
		return "homePage.jsf?faces-redirect=true";
	}

	public String doShowForm() {
		// Pas conseillé à faire. Parce que.
		showForm = showForm ? false : true;
		return null;
	}

	public String doAddOrUpdateClasse() {
		if (classe.getId() != 0) {
			selectedStudent.setClasse(classe);
			studentService.updateStudent(selectedStudent);
			classe.setClassRoom(new ClassRoom());
			classeService.updateClasse(classe);
		} else {
			classe.setClassRoom(new ClassRoom());
			classeService.addClasse(classe);
		}
		return "homePage.jsf?faces-redirect=true";
	}

	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}

	public List<Classe> getClasses() {
		return classes;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public void setClasses(List<Classe> classes) {
		this.classes = classes;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Student getSelectedStudent() {
		return selectedStudent;
	}

	public void setSelectedStudent(Student selectedStudent) {
		this.selectedStudent = selectedStudent;
	}

}
