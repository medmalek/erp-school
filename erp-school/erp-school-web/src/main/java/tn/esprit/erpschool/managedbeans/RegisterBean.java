package tn.esprit.erpschool.managedbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.erpschool.entites.Person;
import tn.esprit.erpschool.entites.Student;
import tn.esprit.erpschool.services.contracts.IStudentCrudLocal;

@ViewScoped
@ManagedBean(name = "registerBean")
public class RegisterBean {

	@EJB
	IStudentCrudLocal studentService;
	private Student person = new Student();

	public Person getPerson() {
		return person;
	}

	public void setPerson(Student person) {
		this.person = person;
	}

	public String doRegister() {
		studentService.addStudent(person);
		return "login?faces-redirect=true";
	}

}
