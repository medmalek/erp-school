package tn.esprit.erpschool.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException
public class NoResultFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
