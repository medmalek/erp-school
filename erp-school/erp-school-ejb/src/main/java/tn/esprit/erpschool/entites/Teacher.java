package tn.esprit.erpschool.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_teacher")
@NamedQueries({
		@NamedQuery(name = "findTeacherByFirstNameAndLastName", query = "select t from Teacher t where t.firstName=:firstName and t.lastName=:lastName"),
		@NamedQuery(name = "findAllTeachers", query = "select t from Teacher t ") })
public class Teacher extends Person implements Serializable {

	private float salary;
	private List<Module> modules;

	public Teacher() {
	}

	public Teacher(String firstName, String lastName, String email,
			String password, Date birthdate, boolean gender, float salary,
			List<Module> modules) {
		super(firstName, lastName, email, password, birthdate, gender);
		this.salary = salary;
		this.modules = modules;
	}

	public Teacher(String firstName, String lastName, String email,
			String password, Date birthdate, boolean gender, float salary) {
		super(firstName, lastName, email, password, birthdate, gender);
		this.salary = salary;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	@OneToMany(mappedBy = "teacher")
	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	private static final long serialVersionUID = 1L;

}
