package tn.esprit.erpschool.entites;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ClassRoom implements Serializable {

	private String labelClassRoom;
	private boolean elevator = false;

	public ClassRoom(String labelClassRoom, boolean elevator) {
		super();
		this.labelClassRoom = labelClassRoom;
		this.elevator = elevator;
	}

	public ClassRoom() {
		// TODO Auto-generated constructor stub
	}

	public String getLabelClassRoom() {
		return labelClassRoom;
	}

	public void setLabelClassRoom(String labelClassRoom) {
		this.labelClassRoom = labelClassRoom;
	}

	public boolean isElevator() {
		return elevator;
	}

	public void setElevator(boolean elevator) {
		this.elevator = elevator;
	}
	
	private static final long serialVersionUID = 1L;

}
