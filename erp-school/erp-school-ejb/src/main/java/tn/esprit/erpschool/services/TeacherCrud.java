package tn.esprit.erpschool.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.erpschool.entites.Teacher;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.ITeacherCrudLocal;
import tn.esprit.erpschool.services.contracts.ITeacherCrudRemote;

@Stateless
public class TeacherCrud implements ITeacherCrudRemote, ITeacherCrudLocal {

	@PersistenceContext
	EntityManager em;

	public void addTeacher(Teacher teacher) {
		em.persist(teacher);
	}

	@Override
	public void updateTeacher(Teacher teacher) {
		em.merge(teacher);
	}

	@Override
	public void deleteTeacher(int id) throws NoResultFoundException {
			em.remove(findTeacherById(id));
	}

	@Override
	public Teacher findTeacherById(int id) throws NoResultFoundException {
		try {
			return em.find(Teacher.class, id);
		}catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public Teacher findTeacherByFirstNameAndLastName(String firstName,
			String lastName) throws MoreThanOneResultException,
			NoResultFoundException {
		TypedQuery<Teacher> query = em.createNamedQuery(
				"findTeacherByFirstNameAndLastName", Teacher.class);
		query.setParameter("firstName", firstName);
		query.setParameter("lastName", lastName);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public List<Teacher> findAllTeachers() {
		TypedQuery<Teacher> query = em.createNamedQuery("findAllTeachers",
				Teacher.class);
		return query.getResultList();
	}
}
