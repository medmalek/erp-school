package tn.esprit.erpschool.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.erpschool.entites.Student;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.IStudentCrudLocal;
import tn.esprit.erpschool.services.contracts.IStudentCrudRemote;

@Stateless
public class StudentCrud implements IStudentCrudRemote, IStudentCrudLocal {

	@PersistenceContext
	EntityManager em;

	public void addStudent(Student student) {
		em.persist(student);
	}

	@Override
	public void updateStudent(Student student) {
		em.merge(student);
	}

	@Override
	public void deleteStudent(int id) throws NoResultFoundException {
			em.remove(findStudentById(id));
	}

	@Override
	public Student findStudentById(int id) throws NoResultFoundException {
		try {
			return em.find(Student.class, id);
		}catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public Student findStudentByFirstNameAndLastName(String firstName,
			String lastName) throws MoreThanOneResultException,
			NoResultFoundException {
		TypedQuery<Student> query = em.createNamedQuery(
				"findStudentByFirstNameAndLastName", Student.class);
		query.setParameter("firstName", firstName);
		query.setParameter("lastName", lastName);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public List<Student> findAllStudents() {
		TypedQuery<Student> query = em.createNamedQuery("findAllStudents",
				Student.class);
		return query.getResultList();
	}
}
