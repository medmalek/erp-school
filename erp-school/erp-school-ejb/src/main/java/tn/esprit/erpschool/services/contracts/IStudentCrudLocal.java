package tn.esprit.erpschool.services.contracts;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.erpschool.entites.Student;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;

@Local
public interface IStudentCrudLocal {

	public void addStudent(Student student);

	public void updateStudent(Student student);

	public void deleteStudent(int id) throws NoResultFoundException;

	public Student findStudentById(int id) throws NoResultFoundException;

	public Student findStudentByFirstNameAndLastName(String firstName,
			String lastName) throws MoreThanOneResultException,
			NoResultFoundException;

	public List<Student> findAllStudents();
}
