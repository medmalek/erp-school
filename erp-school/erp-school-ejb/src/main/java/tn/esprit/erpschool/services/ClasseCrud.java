package tn.esprit.erpschool.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.erpschool.entites.Classe;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.IClasseCrudLocal;
import tn.esprit.erpschool.services.contracts.IClasseCrudRemote;

@Stateless
public class ClasseCrud implements IClasseCrudRemote, IClasseCrudLocal {

	@PersistenceContext
	EntityManager em;

	public void addClasse(Classe classe) {
		em.persist(classe);

	}

	@Override
	public void updateClasse(Classe classe) {
		em.merge(classe);
	}

	@Override
	public void deleteClasse(int id) {
		em.remove(findClasseById(id));
	}

	@Override
	public Classe findClasseById(int id) {
		return em.find(Classe.class, id);
	}

	@Override
	public List<Classe> findAllClasses() {
		TypedQuery<Classe> query = em.createNamedQuery("findAllClasses",
				Classe.class);
		return query.getResultList();
	}

	@Override
	public Classe findClasseByLabel(String label)
			throws NoResultFoundException, MoreThanOneResultException {
		TypedQuery<Classe> query = em.createNamedQuery("findClasseByLabel",
				Classe.class);
		query.setParameter("label", label);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public boolean isClasseLabelUnique(String label) {
		try {
			findClasseByLabel(label);
		} catch (NoResultFoundException e) {
			return true;
		} catch (MoreThanOneResultException e) {
			return false;
		}
		return false;

	}

}
