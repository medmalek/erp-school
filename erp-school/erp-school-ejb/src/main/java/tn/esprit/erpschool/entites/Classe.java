package tn.esprit.erpschool.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_classe")
@NamedQueries({
	@NamedQuery(name = "findClasseByLabel", query = "select c from Classe c where c.label=:label"),
	@NamedQuery(name = "findAllClasses", query = "select c from Classe c ") })
public class Classe implements Serializable {

	private int id;
	private String label;
	private String scholarYear;
	private Curuculum curuculum;

	private List<Student> students;
	private List<Module> modules;
	private ClassRoom classRoom;

	public Classe() {

	}

	public Classe(String label, String scholarYear, Curuculum curuculum) {
		this.label = label;
		this.scholarYear = scholarYear;
		this.curuculum = curuculum;
	}

	public Classe(String label, String scholarYear, Curuculum curuculum,
			ClassRoom classRoom) {
		this.label = label;
		this.classRoom = classRoom;
		this.scholarYear = scholarYear;
		this.curuculum = curuculum;
	}

	public Classe(String label, String scholarYear, Curuculum curuculum,
			List<Student> students, List<Module> modules, ClassRoom classRoom) {
		this.label = label;
		this.scholarYear = scholarYear;
		this.curuculum = curuculum;
		this.students = students;
		this.modules = modules;
		this.classRoom = classRoom;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getScholarYear() {
		return this.scholarYear;
	}

	public void setScholarYear(String scholarYear) {
		this.scholarYear = scholarYear;
	}

	public Curuculum getCuruculum() {
		return curuculum;
	}

	public void setCuruculum(Curuculum curuculum) {
		this.curuculum = curuculum;
	}

	@Embedded
	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

	@OneToMany(mappedBy = "classe", cascade = { CascadeType.MERGE,
			CascadeType.REMOVE })
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@OneToMany(mappedBy = "classe", cascade = { CascadeType.MERGE,
			CascadeType.REMOVE })
	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	private static final long serialVersionUID = 1L;

}
