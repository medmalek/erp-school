package tn.esprit.erpschool.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.erpschool.entites.Person;
import tn.esprit.erpschool.exceptions.BadCredentialsException;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;
import tn.esprit.erpschool.services.contracts.IPersonServiceLocal;
import tn.esprit.erpschool.services.contracts.IPersonServiceRemote;

@Stateless
public class PersonService implements IPersonServiceLocal, IPersonServiceRemote {

	@PersistenceContext
	EntityManager em;

	@Override
	public Person findPersonByCredentials(String email,
			String password) throws BadCredentialsException {
		TypedQuery<Person> query = em.createNamedQuery(
				"findPersonByCredentials", Person.class);
		query.setParameter("email", email);
		query.setParameter("password", password);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException | NoResultException e) {
			throw new BadCredentialsException();
		} 
	}
	
	@Override
	public Person findPersonByfirstNameAndLastName(String firstName,
			String lastName)
			throws MoreThanOneResultException, NoResultFoundException {
		TypedQuery<Person> query = em.createNamedQuery(
				"findPersonByFirstNameAndLastName", Person.class);
		query.setParameter("firstName", firstName);
		query.setParameter("lastName", lastName);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public Person findPersonByEmail(String email)
			throws MoreThanOneResultException, NoResultFoundException {
		TypedQuery<Person> query = em.createNamedQuery(
				"findPersonByEmail", Person.class);
		query.setParameter("email", email);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	@Override
	public boolean isPersonEmailUnique(String email)
			throws MoreThanOneResultException, NoResultFoundException {
		try {
			findPersonByEmail(email);
		} catch (MoreThanOneResultException e) {
			return false;
		} catch (NoResultFoundException e) {
			return true;
		}
		return false;
	}
}
