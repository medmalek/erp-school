package tn.esprit.erpschool.services.contracts;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.erpschool.entites.Classe;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;

@Local
public interface IClasseCrudLocal {

	public void addClasse(Classe classe);

	public void updateClasse(Classe classe);

	public void deleteClasse(int id) throws NoResultFoundException;

	public Classe findClasseById(int id) throws NoResultFoundException;

	public Classe findClasseByLabel(String label)
			throws MoreThanOneResultException, NoResultFoundException;
	
	public boolean isClasseLabelUnique(String label);

	public List<Classe> findAllClasses();

}
