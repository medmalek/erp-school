package tn.esprit.erpschool.services.contracts;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.erpschool.entites.Teacher;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;

@Remote
public interface ITeacherCrudRemote {

	public void addTeacher(Teacher teacher);

	public void updateTeacher(Teacher teacher);

	public void deleteTeacher(int id) throws NoResultFoundException;

	public Teacher findTeacherById(int id) throws NoResultFoundException;
	
	public Teacher findTeacherByFirstNameAndLastName(String firstName, String lastName) throws MoreThanOneResultException, NoResultFoundException;

	public List<Teacher> findAllTeachers();
}
